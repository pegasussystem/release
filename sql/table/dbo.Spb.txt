/****** Object:  Table [dbo].[Spb]    Script Date: 21/10/2020 10:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Spb](
	[CreatedAt] [datetime2](4) NULL,
	[UpdatedAt] [datetime2](4) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[SpbId] [bigint] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NULL,
	[BranchId] [int] NULL,
	[SubBranchId] [int] NULL,
	[SpbNo] [varchar](50) NULL,
	[SpbYearMonth] [int] NULL,
	[SpbGoodsNo] [int] NULL,
	[OriginCityCode] [int] NULL,
	[OriginAreaCode] [int] NULL,
	[DestinationCityCode] [int] NULL,
	[DestinationAreaCode] [int] NULL,
	[TableNameId] [int] NULL,
	[TableName] [varchar](20) NULL,
	[TypesOfGoods] [varchar](20) NULL,
	[Carrier] [varchar](20) NULL,
	[QualityOfService] [varchar](20) NULL,
	[TypeOfService] [varchar](20) NULL,
	[Rates] [decimal](16, 3) NULL,
	[Packing] [decimal](16, 3) NULL,
	[Quarantine] [decimal](16, 3) NULL,
	[Etc] [decimal](16, 3) NULL,
	[Ppn] [decimal](16, 3) NULL,
	[Discount] [decimal](16, 3) NULL,
	[TotalPrice] [decimal](16, 3) NULL,
	[PaymentMethod] [varchar](20) NULL,
	[SenderId] [int] NULL,
	[SenderNameId] [int] NULL,
	[SenderStoreId] [int] NULL,
	[SenderPlaceId] [int] NULL,
	[SenderAddressId] [int] NULL,
	[ReceiverId] [int] NULL,
	[ReceiverNameId] [int] NULL,
	[ReceiverStoreId] [int] NULL,
	[ReceiverPlaceId] [int] NULL,
	[ReceiverAddressId] [int] NULL,
	[Description] [varchar](500) NULL,
	[ManifestId] [bigint] NULL,
	[ViaId] [int] NULL,
	[IsVoid] [bit] NULL,
	[VoidDate] [datetime2](4) NULL,
	[VoidBy] [varchar](50) NULL,
	[VoidReason] [varchar](500) NULL,
 CONSTRAINT [PK__Spb__C386CB41E255D51E] PRIMARY KEY CLUSTERED 
(
	[SpbId] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_Spb_uniquePerCompany] UNIQUE NONCLUSTERED 
(
	[CompanyId] ASC,
	[SpbYearMonth] ASC,
	[SpbGoodsNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Spb] ADD  CONSTRAINT [DF__Spb__CreatedAt__4A8310C6]  DEFAULT (getutcdate()) FOR [CreatedAt]
GO
