/****** Object:  Table [dbo].[CostCarrier]    Script Date: 21/10/2020 10:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CostCarrier](
	[CreatedAt] [datetime2](4) NULL,
	[UpdatedAt] [datetime2](4) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[CostCarrierId] [int] IDENTITY(1,1) NOT NULL,
	[CityOrigin] [varchar](255) NULL,
	[CityDestination] [varchar](255) NULL,
	[CarrierId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CostCarrierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [unique] UNIQUE NONCLUSTERED 
(
	[CityOrigin] ASC,
	[CityDestination] ASC,
	[CarrierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CostCarrier] ADD  DEFAULT (getutcdate()) FOR [CreatedAt]
GO