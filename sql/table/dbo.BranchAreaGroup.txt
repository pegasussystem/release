/****** Object:  Table [dbo].[BranchAreaGroup]    Script Date: 21/10/2020 10:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BranchAreaGroup](
	[CreatedAt] [datetime2](4) NULL,
	[UpdatedAt] [datetime2](4) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[BranchAreaGroupId] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [int] NULL,
	[BranchAreaGroupName] [varchar](50) NULL,
	[BranchArea] [varchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[BranchAreaGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BranchAreaGroup] ADD  DEFAULT (getutcdate()) FOR [CreatedAt]
GO
