/****** Object:  Table [dbo].[Manifest]    Script Date: 21/10/2020 10:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manifest](
	[CreatedAt] [datetime2](4) NULL,
	[UpdatedAt] [datetime2](4) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[ManifestId] [bigint] IDENTITY(1,1) NOT NULL,
	[InsertId] [uniqueidentifier] NULL,
	[CompanyId] [int] NULL,
	[BranchId] [int] NULL,
	[SubBranchId] [int] NULL,
	[ManifestAlphabet] [int] NULL,
	[ManifestNo] [varchar](50) NULL,
	[ManifestIncrement] [int] NULL,
	[TableNameId] [int] NULL,
	[TableName] [varchar](20) NULL,
	[OriginCityCode] [int] NULL,
	[DestinationCityCode] [int] NULL,
	[ManifestGroupId] [bigint] NULL,
	[Carrier] [varchar](20) NULL,
	[IsOpen] [bit] NULL,
 CONSTRAINT [PK__Manifest__6064F36602F0F4CD] PRIMARY KEY CLUSTERED 
(
	[ManifestId] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Manifest] ADD  CONSTRAINT [DF__Manifest__Create__607251E5]  DEFAULT (getutcdate()) FOR [CreatedAt]
GO