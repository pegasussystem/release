/****** Object:  Table [dbo].[CostItem]    Script Date: 21/10/2020 10:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CostItem](
	[CreatedAt] [datetime2](4) NULL,
	[UpdatedAt] [datetime2](4) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[CostItemId] [int] IDENTITY(1,1) NOT NULL,
	[CostId] [int] NOT NULL,
	[CostType] [varchar](50) NULL,
	[Unit] [varchar](50) NULL,
	[Cost] [decimal](10, 4) NULL,
	[CostGroup] [varchar](50) NULL,
	[UseWeight] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[CostItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_CostItem] UNIQUE NONCLUSTERED 
(
	[CostId] ASC,
	[CostType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CostItem] ADD  DEFAULT (getutcdate()) FOR [CreatedAt]
GO