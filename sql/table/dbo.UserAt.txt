/****** Object:  Table [dbo].[UserAt]    Script Date: 21/10/2020 10:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAt](
	[CreatedAt] [datetime2](4) NULL,
	[UpdatedAt] [datetime2](4) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[UserAtId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[TableNameId] [int] NULL,
	[TableName] [varchar](20) NULL,
	[RefreshToken] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserAtId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserAt] ADD  DEFAULT (getutcdate()) FOR [CreatedAt]
GO
