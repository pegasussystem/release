/****** Object:  Table [dbo].[MasterCarrier]    Script Date: 21/10/2020 10:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterCarrier](
	[CreatedAt] [datetime2](4) NULL,
	[UpdatedAt] [datetime2](4) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[MasterCarrierId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NULL,
	[MasterCarrierVendorId] [int] NOT NULL,
	[MasterCarrierName] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[MasterCarrierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_MasterCarrier] UNIQUE NONCLUSTERED 
(
	[MasterCarrierName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MasterCarrier] ADD  DEFAULT (getutcdate()) FOR [CreatedAt]
GO
