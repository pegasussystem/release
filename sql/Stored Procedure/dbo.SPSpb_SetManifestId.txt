/****** Object:  StoredProcedure [dbo].[SPSpb_SetManifestId]    Script Date: 21/10/2020 10:49:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- SPSpb_SetManifestId @SpbId=21773
-- =============================================
CREATE PROCEDURE [dbo].[SPSpb_SetManifestId] 
	@SpbId BIGINT =0
	
AS
BEGIN
	SET NOCOUNT ON;
	 

	 /** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 *  LOGIC
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */
	UPDATE Spb 
	SET ManifestId = (
		SELECT 
		 MIN(m.ManifestId) ManifestId  
		FROM Manifest m
		LEFT JOIN ManifestKoli mk ON mk.ManifestId = m.ManifestId
		LEFT JOIN ManifestKoliSpb mks ON mks.ManifestKoliId = mk.ManifestKoliId
		LEFT JOIN SpbGoods sg ON sg.SpbGoodsId = mks.SpbGoodsId 
		WHERE sg.SpbId = Spb.SpbId
	) 
	WHERE Spb.SpbId = @SpbId

	SELECT 'ok' 'Ok'

END
GO