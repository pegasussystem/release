/****** Object:  StoredProcedure [dbo].[SPUser_Create]    Script Date: 21/10/2020 10:49:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- [SPUser_Create] @CompanyId=1, @Username= 'g2.bpn', @Email= 'g2.bpn', @Password= 'g2.bpn', @TableNameId=7 , @TableName= 'SubBranch'
-- =============================================
CREATE PROCEDURE [dbo].[SPUser_Create] 
	@CompanyId INT =0,
	@Username Varchar(200),
	@Email Varchar(200),
	@Password Varchar(200),
	
	@TableNameId INT,
	@TableName Varchar(200)

	
AS
BEGIN
	SET NOCOUNT ON;

	/** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 *  VARIABLE & SET VARIABLE
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */

	 DECLARE @UserId_ Varchar(200)
	 

	 

	 /** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 *  LOGIC
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */

	INSERT INTO [User] 
	(CompanyId, Username, Email, [Password])
	VALUES (@CompanyId, @Username, @Email, @Password)

	--
	-- get id 
	SELECT @UserId_ = Userid FROM [User] WHERE CompanyId=@CompanyId AND Username=@Username 
		AND Email=@Email AND [Password]=@Password

	--
	-- insert to USERAT
	INSERT INTO UserAt 
		(UserId, TableNameId, TableName)
	VALUES (@UserId_, @TableNameId, @TableName)

	SELECT 'ok' 'Ok'

END
GO