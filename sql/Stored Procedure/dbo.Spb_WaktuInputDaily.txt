/****** Object:  StoredProcedure [dbo].[Spb_WaktuInputDaily]    Script Date: 21/10/2020 10:49:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Spb_WaktuInputDaily]
	@FROM_YEAR_ int = 0,
	@FROM_MONTH_ int = 0,
	@FROM_DAY_ int = 0,

	@TO_YEAR_ int = 0,
	@TO_MONTH_ int = 0,
	@TO_DAY_ int = 0
AS
BEGIN 
		-- EXEC Spb_WaktuInputDaily 2020,3,18,2020,3,19

	SET NOCOUNT ON;

	-- VARIABLE
	-- ///////////////////

	DECLARE @fromDate_ VARCHAR(30), 
        @toDate_ VARCHAR(30);

	-- SET VARIABLE
	-- ///////////////////
	SET @fromDate_ = CONCAT_WS('-', @FROM_YEAR_, @FROM_MONTH_, CONVERT(varchar(10),@FROM_DAY_)+ ' 00:00:00')
	SET @toDate_ = CONCAT_WS('-', @TO_YEAR_, @TO_MONTH_, CONVERT(varchar(10),@TO_DAY_)+ ' 23:59:59')

	-- LOGIC
	-- ///////////////////
	
	SELECT -- GROUP
		b.Username,
		COUNT(1) 'TotalSpb', 
		CONVERT(decimal(6,4), 3600*1.0/AVG(Detik)) AS 'TotalSpbAvgPerHour',
		 
		CONVERT(varchar, DATEADD(second, SUM(Detik), 0), 108) AS 'MinuteTotal',
		CONVERT(varchar, DATEADD(second, AVG(Detik), 0), 108) AS 'MinuteAvg',
		CONVERT(varchar, DATEADD(second, MAX(Detik), 0), 108) AS 'MinuteMax',
		CONVERT(varchar, DATEADD(second, MIN(Detik), 0), 108) AS 'MinuteMin',
		SUM([S1_30]) as 'S1_30', 
		SUM([S31_60]) as 'S31_60',
		SUM([S61_90]) as 'S61_90',
		SUM([S91_120]) as 'S91_120',
		SUM([S121_150]) as 'S121_150',
		SUM([S151_180]) as 'S151_180',
		SUM([S181_210]) as 'S181_210',
		SUM([S211_240]) as 'S211_240',
		SUM([S241_270]) as 'S241_270',
		SUM([S271_300]) as 'S271_300',
		SUM([S301_330]) as 'S301_330',
		SUM([S331_360]) as 'S331_360',
		SUM([S361_390]) as 'S361_390',
		SUM([S391_420]) as 'S391_420',
		SUM([S421_450]) as 'S421_450',
		SUM([S451_480]) as 'S451_480',
		SUM([S481_510]) as 'S481_510',
		SUM([S511_540]) as 'S511_540',
		SUM([S541_570]) as 'S541_570',
		SUM([S571_600]) as 'S571_600',
		SUM([S601_]) as 'S601_'

		 -- , b.OriginCityCode, b.OriginAreaCode, b.DestinationCityCode, b.DestinationAreaCode, b.Carrier
	FROM (
		
		SELECT -- LOCAL DATE
			CreatedAtLocal,
			 DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) AS 'Detik',
			-- CONVERT(varchar, DATEADD(second, DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal), 0), 108) AS 'Menit',
			 u.Username,
			-- t.SpbNo, 
			t.OriginCityCode, t.OriginAreaCode, t.DestinationCityCode, t.DestinationAreaCode,
			t.Carrier,
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal)  <= 30 THEN 1 ELSE 0		END 'S1_30',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=31 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=60 THEN 1 ELSE 0 END 'S31_60',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=61 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=90 THEN 1 ELSE 0 END 'S61_90',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=91 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=120 THEN 1 ELSE 0 END 'S91_120',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=121 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=150 THEN 1 ELSE 0 END 'S121_150',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=151 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=180 THEN 1 ELSE 0 END 'S151_180',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=181 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=210 THEN 1 ELSE 0 END 'S181_210',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=211 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=240 THEN 1 ELSE 0 END 'S211_240',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=241 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=270 THEN 1 ELSE 0 END 'S241_270',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=271 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=300 THEN 1 ELSE 0 END 'S271_300',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=301 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=330 THEN 1 ELSE 0 END 'S301_330',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=331 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=360 THEN 1 ELSE 0 END 'S331_360',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=361 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=390 THEN 1 ELSE 0 END 'S361_390',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=391 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=420 THEN 1 ELSE 0 END 'S391_420',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=421 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=450 THEN 1 ELSE 0 END 'S421_450',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=451 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=480 THEN 1 ELSE 0 END 'S451_480',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=481 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=510 THEN 1 ELSE 0 END 'S481_510',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=511 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=540 THEN 1 ELSE 0 END 'S511_540',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=541 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=570 THEN 1 ELSE 0 END 'S541_570',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=571 AND DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) <=600 THEN 1 ELSE 0 END 'S571_600',
			CASE WHEN DATEDIFF(ss,CreatedAtLocal, UpdatedAtLocal) >=601 THEN 1		ELSE 0 END 'S601_'


			-- t.IsVoid
		FROM (
			SELECT 
				DATEADD(HOUR,7,CreatedAt ) CreatedAtLocal,
				DATEADD(HOUR,7,UpdatedAt ) UpdatedAtLocal,
				*
			FROM Spb
			WHERE UpdatedAt IS NOT NULL
		) as t
		LEFT JOIN [User] u ON u.UserId = t.CreatedBy
		WHERE CreatedAtLocal BETWEEN 
		@fromDate_ AND @toDate_
		-- WHERE YEAR(CreatedAtLocal) = @FROM_YEAR_ AND MONTH(CreatedAtLocal) = @FROM_MONTH_ AND DAY(CreatedAtLocal) = @FROM_DAY_
		--WHERE YEAR(CreatedAtLocal) = 2020 AND MONTH(CreatedAtLocal) = 3 AND DAY(CreatedAtLocal) = 18
			--AND Username='agus.setiawan.sb1'
	
	) as b
	 GROUP BY b.Username -- , b.OriginCityCode, b.OriginAreaCode, b.DestinationCityCode, b.DestinationAreaCode, b.Carrier
	 ORDER BY AVG(Detik) ASC
END
GO